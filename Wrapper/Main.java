public class Main{

/**
 * Wrapper class
 * IntegerやDoubleなどの事を指す。
 * int -> Integer　
 * double -> Double
 * 
 * 
 * 
 * Javaには、基本データ型と参照型がある。
 * int,long,short,byte,double,float,boolean,charは基本データ型(プリミティブ型)
 * 上記以外は、参照型になる。
 * 
 * 基本データ型にはnullは使用できない。
 * 参照型のデータには、値そのものではなく、値が入っているメモリの領域の場所が入る。
 * どの場所も指し示していない状態にしたい場合は、nullを使用できる。
 * 
 * 
 * Javaのクラスによっては、参照型しか受け付けないものもあるので、
 * 相互に変換できるようにしておく。
 * 
 * ラッパークラスにすることによって、便利な定数やメソッドを使えるようになる。
 * 
 */

    public static void main(String[] args){

        /* 基本データ型を、ラッパークラスに変換 */
        // Integer i = new Integer(32);

        /* ラッパークラスの値を、基本データ型に変換 */
        // int n = i.intValue();

        /* 上記の短縮 */
        Integer i = 32; //auto boxing  自動変換を"参照型"に入れる
        i = null;

        int n = i; // auto unboxing 自動変換を"基本データ型"に入れる。
        
        System.out.println(i);
    }
}