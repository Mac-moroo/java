class ForIf{
	public static void main(String[] args){
		boolean failure = false;
		int scores = {43,75,86,41,98};
		for(int score : scores){
			if(score < 60){
				failure = true;
				break;
			}
		}
		if(failure){
			System.out.println("赤点です。");
		}
	}
}