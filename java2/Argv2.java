import java.util.Scanner;
import java.util.InputMismatchException;
class Argv2{
	public static void main(String[] argv){
		try(Scanner stdIn = new Scanner(System.in);){
		int x;
		do{
			System.out.print("0=true/1=false : ");
			x = stdIn.nextInt();
		}while (x < 0 || x > 11);
		boolean a;
		if(x == 0){
			a = true;
		}else{
			a = false;
		}
		System.out.println("論理型変数に"+ a + "を代入しました。");
	}catch (InputMismatchException e){
		System.out.println("不正な入力値です");
	}
}
}