import java.util.Scanner;
import java.lang.NumberFormatException; //NumberFormatExceptionというエラーが出たため、import

class TryCatch{
	public static void main(String[] args) throws Exception{
		System.out.println(args[0]); //起動変数[0]を出力
		int startArgv = Integer.parseInt(args[0]); //起動引数を整数型に変換

		double rand = Math.random() * startArgv + 1; //浮動小数点型randにランダムで起動変数+1の値を代入する。※ Math.randomは複数型なので、浮動小数点型。
		int inputNumberA = (int)rand; //浮動小数点型randを整数型inputNumberAに代入
		if(startArgv <= 0){ //もし起動変数が0以下だった場合、エラーを出力
			System.out.println("エラーです。1以上を入力してください");
		}else{ //それ以外だった場合、inputNumberAに(ランダムで)数値が出力される。
			System.out.println("数値A:"+inputNumberA);

			try(Scanner scanner = new Scanner(System.in)){ //Scannerを使う準備
				Integer inputNumberB = null; //Integer型のinputNumberBを宣言。 = null(何も入っていない)とする。


				int numberOfTimes = 0 ; // 回数の初期化
				while(inputNumberB == null || inputNumberA != inputNumberB){ //inputNumberBの中に整数型が入っていなかった場合 または、inputNumberAとinputNumberBが等しくなかった場合、正解には向かわず、繰り返し処理を行う
					if(inputNumberB != null && inputNumberA < inputNumberB){ //inputNumberBの中に整数型が入っていた場合、かつ、inputNumberA 大なり　inputNmuberBの場合
						System.out.println("BIG");
					}else if(inputNumberB != null && inputNumberA > inputNumberB){ //inputNumberBの中に整数型が入っていた場合、かつ、inputNumberA小なりinputNumberBの場合
						System.out.println("SMALL");
					}
					System.out.print("次の数値を入力してください > ");
					try{ //if文ではなく、try catch文を使う理由は、例外処理を行いたいため。
						String input = scanner.next(); //※nextIntを使うと、無限ループが止まらなくなるため、nextを文字列型に代入し、inputに代入
						inputNumberB = Integer.parseInt(input); //文字列型input変数を整数型に変換し、inputNumberBに代入
						numberOfTimes++; //回数の繰り返し
					}catch(NumberFormatException e){ //例外処理
						System.out.println("不正な入力値です");
					}
				} //ここでwhile文が終了し、繰り返し処理に戻る
					System.out.println("COLLECT!");
					System.out.println("入力回数は"+ numberOfTimes +"回でした。");
			}
		}
	}
}
/*
try catch 例外処理

・新たなメソッド

|| または
&& かつ
*/