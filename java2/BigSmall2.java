 

class BigSmall2{
	public static void main(String[] args) throws Exception{
		Scanner scanner = new Scanner(System.in);

		System.out.println(args[0]);
		int startArgv = Integer.parseInt(args[0]); //起動引数

		int randomCount =(int) (Math.random() * (startArgv + 1)); //起動引数からランダムな整数

		if(startArgv <= 0){
			System.out.println("エラーです！　1以上を入力してください！");
		}else{
			System.out.println("ランダムで" + randomCount + "が生成されました！");

	//----------起動引数 fin--------
	//----------ユーザーの入力値---
			int count = 0;
			while(true){
				try{
					System.out.print("数値を入力してください > ");
					String coversionInputNumber = scanner.next();
					int inputNumber = Integer.parseInt(coversionInputNumber);
					if(inputNumber < randomCount){
						System.out.println("SMALL");
						count ++;
					}else if(inputNumber > randomCount){
						System.out.println("BIG");
						count ++;
					}else{
						System.out.println("COLLECT!");
						count ++;
						break;
					}
				}catch(NumberFormatException e){
					System.out.println("不正な入力値です");
				}

			}
			System.out.println("入力回数は" + count + "回でした！");
		}
	}
}