import java.util.Scanner;
import java.util.InputMismatchException;
class Argv{
	public static void main (String[] args) throws Exception{
		System.out.println(args[0]);
		int StartArgv = Integer.parseInt(args[0]); //起動引数を整数型に変換

		double rand = Math.random() * StartArgv + 1;
		int inputNumberA = (int)rand;
		if(StartArgv <= 0){
			System.out.println("エラーです。1以上を入力してください");
		}else{
			System.out.println("数値A:"+inputNumberA);
		// 0〜数値Aまでのランダムな数値の出力

			System.out.print("数値Aと比較する数値Bの値を入力してください > ");
			Scanner scanner = new Scanner(System.in);
			String input = scanner.next();
			int inputNumberB = Integer.parseInt(input);
//

//---数値Aと数値Bの比較の繰り返し処理開始---
			int NumberOfTimes= 1 ; // 回数の初期化
			while(inputNumberA < inputNumberB || inputNumberA > inputNumberB){
				if(inputNumberA < inputNumberB){
					System.out.println("BIG");
					System.out.println("次の数値を入力してください");
					input = scanner.next();
					inputNumberB = Integer.parseInt(input);
				}else if(inputNumberA > inputNumberB){
					System.out.println("SMALL");
					System.out.println("次の数値を入力してください");
					input = scanner.next();
					inputNumberB = Integer.parseInt(input);
				}
				NumberOfTimes++;
			}
			if (inputNumberA == inputNumberB){
				System.out.println("COLLECT!");
				System.out.println("入力回数は"+ NumberOfTimes +"回でした。");
			}
		}
	}
}


/*
条件式：もし数値Aより数値Bの方が大きかったら"BIG"と出力、数値Aより数値Bの方が小さいかったら"SMALL" trueで任意の処理をし、繰り返す。
条件式の時、同じ数値になった場合、falseになり、繰り返しが終わる。



プレイヤーが入力した値が数値でなかった場合、入力ミスと表記し、再入力を促す。(繰り返し処理を続行させる) => true
プレイヤーが入力した値が数値だった場合、繰り返し処理から抜け出す => false
*/