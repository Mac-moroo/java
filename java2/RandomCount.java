class RandomCount{
	public static void main(String[] args) throws Exception{

		System.out.println(args[0]);  //起動引数
		int startArgv = Integer.parseInt(args[0]) + 1;
		int randomCount =(int) (Math.random() * startArgv); //ランダム整数

		if(startArgv <= 0){
			System.out.println("エラーです！　1以上を入力してください！");
		}else{
			System.out.println("ランダムで" + randomCount + "が生成されました！");
		}
	}
}