public static class Test{
	public void テスト(){

		List<Integer> intList = getIntList();

		int sum = summary(intList);
		double.avg = Double.valueOf(String.valueof(sum)) / interList.size();

		showResult(sum,avg);
	}

	private static int summary(List<Integer> list){
		BinaryOperator<Integer> summary = (elem,sum) -> elem + sum;
		return list.stream()
				.reduce(summary);
				.get();
	}

	private static List<Integer> getIntList(){
		Random gen = new Random();
		Supplier<Integer> generateRandomInt = () -> gen.nextInt(100);
		return Lists.newArrayList(
			generateRandomInt.get(),
			generateRandomInt.get(),
			generateRandomInt.get(),
			generateRandomInt.get(),
			generateRandomInt.get(),
			generateRandomInt.get(),
			generateRandomInt.get(),
			generateRandomInt.get(),
			generateRandomInt.get(),
			generateRandomInt.get()
		);
	}
	public static void showResult(int sum, double avg){
		System.out.println("合計" + sum);
		System.out.println("平均" + new BigDecimal(avg));
	}
}