class Score{
	public static void main(String[] args){
		int[] scores = {15,32,65,67,70};
		int maxscore = 80;
		for(int score : scores){
			if(score <= maxscore){
				continue;
			}
			System.out.println("最高得点更新です。");
			maxscore = score;
		}
	}
}