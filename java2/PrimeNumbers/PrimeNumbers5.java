class PrimeNumbers5 {
    public static void main( String args[] ) {
      // String型のコマンドライン引数をint型に変換
      int num = Integer.parseInt(args[0]);
  
      if (num != 1) {
        // 判定用の変数
        boolean prime = true;
        // 入力された文字を2から割っていく。
        // その余りが0になったら1と入力された数自身以外で割れたことになる。
        // その場合はprimeにfalseを入れる
        for (int i = 2; i <= num - 1; i++) {
          if ((num % i) == 0) {
            prime = false;
          }
        }
        if (prime == true) {
          // 割り切れなかった(余り0にならない)場合
      System.out.println("素数だよー");
        } else {
      // 割り切れた場合
      System.out.println("素数じゃないよー");
        }
      } else {
        // 1は素数ではないので
        System.out.println("1かよ！");
      }
    }
  }