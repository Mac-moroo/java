class PrimeNumbers{
	public static void main (String[]args) throws Exception{
		int inputNum = Integer.parseInt(args[0]); //起動引数

		boolean prime = true;
		for(int i = 2;i<inputNum-1;i++){
			if(inputNum % i ==0){
				prime = false;
			}
		}
		if(prime == true){
			System.out.println("素数");
		}else{
			System.out.println("素数ではない");
		}
	}
}
// 素数は、1と自分自身以外の数値では割れない