class Main{
    public static void main (String[]args){
        int x=5;
        int[] y = new int[10];
        isPrime(x,y);
        System.out.println(y.length);
    }
    
    
    static boolean isPrime(int target, int[] smallerPrimes){ //真偽型 inPrimeメソッドの引数に整数型　target、整数型 smallerprimesを宣言
        for(int smallerPrime : smallerPrimes){ //smallerPrime変数には、反復するごとにコレクション(複数のデータを保存する)という意味。配列に似)の中身が処理を終えるまで代入される。
            if(smallerPrime != 0 && target % smallerPrime == 0){ //もし
                return false;
            }
        }return true;
    }
}