class PrimeNumbers4{
	public static void main(String[] args) throws Exception{
		//フィールド
		int index = 0; //primeIndexを0とする。
		int num = Integer.parseInt(args[0]);
		int[] arrayPrimes = new int[num]; //num個の配列を生成 → arrayPrimes
		
		for(int target = 2; target <= arrayPrimes.length; target++){ // 繰り返し処理　2 <= arrayPrimesの配列の数まで 例)10だったら2,3,4,5,6,7,8,9,10
			if(isPrime(target,arrayPrimes)){ //isPrimeメソッドは0以外かつ、割り切れるのをfalseにする処理。
				arrayPrimes[index++] = target; //2<=arrayPrimes.lengthで繰り返し処理の中で、割り切れないもの(true)を表示する。
				System.out.println(target); //targetは、2<=arrayPrimes.lengthの連続した数値を表示
			}
		}
		System.out.println(num+"以下の素数はこの"+ index +"つです");
	}
	//処理終了

	//1つ1つの素数をtrueかfalseで判定している。
	static boolean isPrime(int target, int[] arrayPrimes){
        for(int arrayPrime : arrayPrimes){ 
            if(arrayPrime != 0 && target % arrayPrime == 0){ 
                return false;
            }
        }return true;
	}
	/*
	配列arrayPrimesの値が、arrayPrimeに繰り返し入る。
	もしarrayPrimeの値が0でない時、または	
	*/
}


/*
素数の数え挙げ
配列には0が入っているため、0を無くしてあげる必要がある。

起動引数から2以上の整数を入力すると、入力した整数以下の素数を小さい順にすべて表示し、
また見つけた素数の数も合わせて表示する。

ここでやったことを順番に
・検討
・一つひとつの素数を判定
・今の判定を使って全ての素数を判定
・最後に表示


もし最初の1個目の素数の場合だったら
カンマをつけない
それ以外の場合は、
カンマをつける。

*/


