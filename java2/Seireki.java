class Seireki{
	public static void main(String[] args){
		String string = "1867";
		int seireki = Integer.parseInt(string);

		if(seireki <= 1867){
			System.out.println("明治より昔");
		}else if(seireki >= 2020){
			System.out.println("平成より未来");
		}else if(seireki >= 1989){
			System.out.println("平成");
		}else if(seireki >= 1926){
			System.out.println("昭和");
		}else if(seireki >= 1912){
			System.out.println("大正");
		}else if(seireki >= 1868){
			System.out.println("明治");
		}
	}
}