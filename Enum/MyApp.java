/*列挙型
 * 列挙型を定義すると、ordinal()という特殊なメソッドも定義されて、0から始まる連番がセットされる
 * 列挙型の中にフィールドやメソッドを持つことができる。
*/
enum Result{
    SUCCESS, //0
    ERROR,
}

public class MyApp{
    public static void main(String[] args) {
        Result res; //Result型のres変数

        res = Result.ERROR; 

        switch (res){
            case SUCCESS:
                System.out.println("OK"); 
                System.out.println(res.ordinal());  
                break;
            case ERROR:
                System.out.println("NO");
                System.out.println(res.ordinal());  
                break;
        }
    }
}