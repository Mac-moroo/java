/**
 * Thread　スレッド処理
 * コンピュータの処理単位のことで、これを複数立ち上げれば複数の処理を同時に実行せることが出来る。
 * 
 * Thread処理を実行するには、Threadクラスのインスタンスを作ってあげて、startメソッドを呼ぶ必要がある。
 * GUIなどが絡むアプリを作るようになると、この知識が必要になってくる。
 */
class MyRunnable implements Runnable{
    
    @Override
    public void run(){
        for(int i = 0;i<500;i++){
            System.out.print("*");
        }
    }
}

public class Main{
    public static void main(String[] args) {
        MyRunnable r = new MyRunnable();
        Thread t = new Thread(r);
        t.start();

        for(int i = 0; i < 500;i++){
            System.out.print('.');
        }
    }
}