/*
 *例外処理 

 *tryの中には、例外が発生しそうな処理を記述
 *catchの中には、
 *finallyの中には、例外が発生しようが発生しまいが、最後の後処理を行う時に記述

 *System.err.println(e.getMessage());の「err」はエラー出力用
*/


public class Main{
    public static void div(int a, int b){

        try{
            if(b < 0){
                throw new MyException("not minus");
            }
            System.out.println(a / b);
        }catch(ArithmeticException e){ // 0除算の時に投げる
            System.err.println(e.getMessage());
        }catch(MyException e){ 
            System.err.println(e.getMessage());
        }finally{
            System.out.println("---end---");
        }
    }

    public static void main(String args[]){
        div(3, 0);
        div(5, -2);
        div(10, 5);
    }
}