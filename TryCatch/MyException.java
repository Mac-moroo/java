/*
 * 独自の例外を設定する方法
 * 
 * コンストラクタでエラーメッセージを受け取ることができる。
*/

class MyException extends Exception{
    public MyException(String s){
        super(s);
    }
}