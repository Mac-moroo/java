class Main{
	public static void main(String[] args){
		Person worker1 = new Person("エルヴィン・フォン・ベンツ");
		worker1.introduction();

		Person worker2 = new Person("内田貴",64);
		worker2.introduction();

		Person worker3 = new Person("柴田芳樹",59);
		worker3.introduction();
	}
}

/*
コンストラクタではなく、オーバーロードを使用
PersonやWorkerは抽象メソッド　　　抽象メソッドも使用する
*/