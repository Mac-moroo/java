class User{
    String name = "モロオ"; //nameという文字列型の変数を指定する。
    int age = 24;

    //コンストラクタ
    User(String name,int age){
        this.name = name;
        this.age = age;
    }

    //自己紹介メソッド
    void introDuce(){
        System.out.println(name);
        System.out.println(age);
    }
    //挨拶メソッド
    void sayHi(){
        System.out.println("こんにちは"+name);
    }

}