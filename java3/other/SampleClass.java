public class SampleClass{
	public SampleClass(){
		System.out.println("デフォルトコンストラクタです");
	}
	SampleClass(String str){
		System.out.println("引数ありコンストラクタです。引数「" + str + "」が渡されました。");
		
	}
}