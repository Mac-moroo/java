class ForIndex{
    public static void main(String[]args){
        String[] names = new String[]{"Aさん","Bさん","Cさん","Dさん"};
        int[] scores = new int[]{82,43,55,32};

        for(int i=0; i<names.length; i++){
            System.out.println(names[i]+"の点数は"+scores[i]);
        }
    }
}