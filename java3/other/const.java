public class Human{
	String name;
	int birthday;
	int manpukudo;

	Human(){
		Human 
		this.name = "ノブオ";
		this.birthday = 19970101;
		this.manpukudo = 50;
	}

	void eat(){
		this.manpukudo += 60;
	}

}
/*
コンストラクタとは、newを使用して、インスタンスを生成した後に自動的に呼び出される特別なメソッドのこと。
定義
① コンストラクタ名とはクラス名と同じにする
② 戻り値を書いてはいけない(voidも書かない)
*/