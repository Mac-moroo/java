class User{
    String name = "モロオ"; //nameという文字列型の変数を指定する。
    int age = 24;

    //コンストラクタ
    User(String name,int age){
        this.name = name;
        this.age = age;
    }

    //メソッド
    void introDuce(){
        System.out.println(name);
    } 
}