class BigorSmall implements QuizGame{
    
    /** 入力数値 */
    private int inputNumber;
    /** ランダムカウント*/
    private int randomCount;
    /** ランダムカウントを100までに制限*/
    private static final int SEIGEN_RANDOM_COUNT = (100+1);
    /** 入力回数 */
    private int inputCountTime = 0;
    /** 回答できる入力回数 */
    private static final int CAN_INPUT_COUNT_TIME = 5;

    /**
     * コンストラクタ
     * 最初の入力時、ランダムな値を出力する。
     */
    
    public BigorSmall(){
        random();
    }

    /**
     * 問題文を出力する。
     * @return 問題文
     */
    @Override
    public String question(){
        return "BigOrSmall ! Up to 5 inputs possible！";
    }
    
    /**
     * ランダムカウントと入力数値を比較する。
     * @param input 入力
     */
    
    @Override
    public void answer(String input){
        this.inputNumber = Integer.parseInt(input);
        if(this.inputNumber > this.randomCount){
            System.out.println("BIG");
        }else if(this.inputNumber < this.randomCount){
            System.out.println("SMALL");
        }
        inputCountTime++;
    }
    /**
     * 入力できる回数が5回ずつ、かつ入力回数が5回以上である
     * 入力数値とランダムカウントが同じ
     * 
     * @return ゲームオーバーの条件を満たす場合にtrueを返し、そうでない場合にfalseを返す
     */
    
    @Override
    public boolean gameOver(){
        if(inputCountTime % CAN_INPUT_COUNT_TIME == 0 && inputCountTime >= CAN_INPUT_COUNT_TIME){
            random();
            System.out.println("So bad !!!　Try again !!! Up to 5 inputs possible！");
            return false;
        }else{
            return this.inputNumber == this.randomCount;
        }
    }
    /**
     * ランダムなカウントを返す。
     * @return ランダムカウント
     */
    private void random(){
        this.randomCount = (int) (Math.random()*SEIGEN_RANDOM_COUNT);
    }
}