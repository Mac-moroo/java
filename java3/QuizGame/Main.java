import java.util.Scanner;
import java.util.Random;
public class Main {
    public static void main(String[] args) {
        
        int num = new Random().nextInt(QuizGameList.length());
    	QuizGame quiz = QuizGameList.get(num);
        Scanner scanner = new Scanner(System.in);
        do {
            // 問題文の出力
            System.out.println(quiz.question());
            // 入力を受け付け、それでクイズへ回答
            String input = scanner.next();
            quiz.answer(input);
            
            // ゲームオーバーでない限り繰り返す
        } while( ! quiz.gameOver());
        
        System.out.println("game over! thank you for playing!");
    }
}
