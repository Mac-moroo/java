class QuizGameList {
	static QuizGame[] list = new QuizGame[]{
		// new FlyingPan(),
		// new MillionaireSkytree(),
		new BigorSmall()
	};
	static int length() {
		return list.length;
	}
	static QuizGame get(int num) {
		return list[num];
	}
}