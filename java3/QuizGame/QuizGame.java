interface QuizGame {
    
    // 出題分、その他プライヤーの入力を促すような文字列を返す
    String question();

    // プレイヤーからの回答を受け付ける
    void answer(String input);
    
    // 入力された回答によりこのゲームが完了とみなされる場合にtrueを返し、そうでない限りfalseを返す
    boolean gameOver();
}