public class Main{
    public static void main(String[]args){
        Worker worker1 = new Doctor("エルヴィン・フォン・ベルツ",22);
        worker1.Introduction();
        Worker worker2 = new Scholar("内田貴",64);
        worker2.Introduction();
        Worker worker3 = new Engineer("柴田芳樹",59);
        worker3.Introduction();
    }
}