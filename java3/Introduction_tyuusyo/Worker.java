public abstract class Worker{
    //フィールドの設定
    public String name;
    public int age;

    //抽象メソッド
    abstract void Introduction();

}