public abstract class Worker{
    //フィールドの設定
    private String name;
    private int age;

    public Worker(String n){
        name = n;
        System.out.print(name+"です。");
    }
    public Worker(String n,int a){
        name = n;
        age = a;
        System.out.print(name+"です。年齢は"+age+"才、");
    }

    //抽象メソッド
    abstract void introduction();
}