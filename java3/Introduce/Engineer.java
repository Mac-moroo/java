public class Engineer extends Worker{    
    public Engineer(String n){
        super(n);
    }
    public Engineer(String n,int a){
        super(n,a);
    }
    @Override
    public void introduction(){
        System.out.println("職業はエンジニアです。");
    } 
}