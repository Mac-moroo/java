//魔法使いクラス 攻撃魔法を使うことができる
public class Wizard extends Human{

    public Wizard(){ //コンストラクタ
        super.setName("まほうつかい");
        super.setGender(1);
        super.setLength(170);
        super.setWeight(60);
        super.setVitality(20);
        super.setMagic(50);
    }
    
    public void magicAttack(Human target){
        String name = super.getName();
        super.setVitality(super.getVitality()-20);
        System.out.println(name + "のこうげき");
        System.out.println(name + "は こうげきじゅもん　を　唱えた");

        target.setVitality(target.getVitality()-20);
        System.out.println(target.getName() + "に 20のダメージ をあたえた！");

        super.setMagic(super.getMagic()-10);

        System.out.println(name + "の まりょくは" + super.getMagic()+"になった！");
        System.out.println("");
    }
}