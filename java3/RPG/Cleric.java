//僧侶クラス　回復魔法が使える
public class Cleric extends Human{

    public Cleric(){ //コンストラクタ
        super.setName("そうりょ");
        super.setGender(2);
        super.setLength(160);
        super.setWeight(50);
        super.setVitality(20);
        super.setMagic(70);
    }
    //回復呪文
    public void heallingMagic(Human target){
        String name = super.getName();
        System.out.println(name + "は かいふくまほう　を　となえた！");
        target.setVitality(target.getVitality() + 20);
        System.out.println(target.getName() + "のたいりょくは"+target.getVitality()+"に なった！");

        target.setMagic(super.getMagic() - 10);
        System.out.println(name+"のまりょくは"+super.getMagic()+"に なった！");
        System.out.println("");
    }
}