//  勇者クラス　必殺技を使うことができる
public class Yuusya extends Human{
    //Humanクラスを継承したクラスで、勇者特有の必殺技メソッドを持つ
    public Yuusya(){ //コンストラクタ
        super.setName("ゆうしゃ");
        super.setGender(1);
        super.setLength(180);
        super.setWeight(70);
        super.setVitality(100);
        super.setMagic(10);
    }
    //必殺技メソッド
    public void specialAttack(Human target){
        String name = super.getName();
        super.setVitality(super.getVitality()-20);
        System.out.println(name+"のこうげき");
        System.out.println(name+"の必殺技が　さくれつした！");

        target.setVitality(target.getVitality()-50);
        System.out.println(target.getName()+"に 50　のダメージを　あたえた！");

        super.setVitality(super.getVitality()-10);
        System.out.println(name + "のたいりょくは"+super.getVitality()+"になった");
        System.out.println("");
    }
}
