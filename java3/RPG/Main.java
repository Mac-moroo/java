public class Main{
    public static void main(String[]args){
        Yuusya yuusya = new Yuusya();
        Wizard wizard = new Wizard();
        Cleric cleric = new Cleric();
        LastBoss lastBoss = new LastBoss();

        //敵があらわれた
        System.out.println(lastBoss.getName()+"があらわれた！");

        //バトル開始
        yuusya.specialAttack(lastBoss);
        wizard.magicAttack(lastBoss);
        lastBoss.specialEvilAttack(yuusya);
        cleric.heallingMagic(yuusya);

        //かいふくアイテムを使用
        System.out.println(yuusya.getName()+"は やくそう　を　つかった！");
        yuusya.eatFood("やくそう");
        System.out.println(wizard.getName()+"は まほうのみず　を　つかった！");
        wizard.eatFood("まほうのみず");
        System.out.println("");

        //ステータス表示
        showStatus(yuusya);
        showStatus(wizard);
        showStatus(cleric);
        showStatus(lastBoss);

    }
    //ステータス表示メソッド
    private static void showStatus(Human target){
        System.out.println("- "+target.getName()+"の ステータス---");
        if(target.getGender() == 1){
            System.out.println("男");
        }else{
            System.out.println("女");
        }
        System.out.println("しんちょう : "+target.getLength());
        System.out.println("たいじゅう : "+target.getWeight());
        System.out.println("たいりょく : "+target.getVitality());
        System.out.println("まりょく : "+target.getMagic());
        System.out.println("");
    }
}