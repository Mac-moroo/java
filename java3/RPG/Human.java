public class Human{
    //フィールドの設定
    private String name = null; //名前
    private int gender = 0; //性別(1:男、2:女)
    private double length = 0; //身長
    private double weight = 0; //体重
    private int vitality = 0; //体力
    private int magic = 0; //魔力

    public Human(){ //コンストラクタ
    }
    //getter setter(名前)
    public String getName(){
        return name;
    }
    public void setName(String name){
        this.name = name;
    }
    //getter setter(性別)
    public int getGender(){
        return gender;
    } 
    public void setGender(int gender){
        this.gender = gender;
    }
    //getter settter(身長)
    public double getLength(){
        return length;
    }
    public void setLength(double length){
        this.length = length;
    }
    //getter setter(体重)
    public double getWeight(){
        return weight;
    }
    public void setWeight(double weight){
        this.weight = weight;
    }
    //getter setter(体力)
    public int getVitality(){
        return vitality;
    }
    public void setVitality(int vitality){
        this.vitality = vitality;
    }

    public int getMagic(){
        return magic;
    }
    public void setMagic(int magic){
        this.magic = magic;
    }

    //話すメソッド
    public void talk(String about){
        System.out.println(about);
    }
    //食べるメソッド
    public void eatFood(String food){
        int foodType = 0;
        if(food == "やくそう"){
            foodType = 1;
        }else if(food == "まほうのみず"){
            foodType = 2;
        }else{
            foodType = 3;
        }
        digestFood(foodType);
    }
    //消化メソッド
    private void digestFood(int foodType){
        if(foodType == 1){ //やうそうだったら
            vitality += 10; //体力を10回復
        }else if(foodType == 2){ //まほうのみずだったら
            magic += 10; //魔力を10回復
        }else{ //それ以外だったら
            vitality += 1; //体力を1回復
        }
    }
}
