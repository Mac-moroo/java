//ラスボスクラス　必殺技を放つ
public class LastBoss extends Human{

    public LastBoss(){
        super.setName("ラスボス");
        super.setGender(1);
        super.setLength(210);
        super.setWeight(120);
        super.setVitality(200);
        super.setMagic(40);
    }
    //必殺技メソッド
    public void specialEvilAttack(Human target){
        String name = super.getName();
        System.out.println(name+"のこうげき");
        System.out.println(name+"のじゃあくな 必殺技　が　さくれつした！");

        target.setVitality(target.getVitality()-30);
        System.out.println(target.getName()+"は 30 のダメージ　を　受けた！");

        super.setVitality(super.getVitality()-10);
        System.out.println(name+"のたいりょくは"+super.getVitality()+"になった");
        System.out.println("");
    }

}