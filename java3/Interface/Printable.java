    /*--インターフェース--
    *定数
        ・public static finalは、省略できるので、"大文字"で変数を書けばOK。
    *抽象メソッド
        ・public abstractは、省略できるので、voidのみでOK。
    *dafaultメソッド
        ・抽象メソッドとは違って、直接実装が書けるメソッドになる。
    *staticメソッド
    を書くことができる。

    クラスの継承と違って、インターフェースはクラスに幾つでも適用できるので、柔軟にクラスの拡張することができる。
    */
interface Printable{
    //定数
    double VERSION = 1.2;
    //抽象メソッド
    void print();
    //defeultメソッド
    public default void getInfo(){
        System.out.println("I/F ver. " + Printable.VERSION);
    }
}