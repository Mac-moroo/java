public abstract class Vehicle{ //乗り物クラス
    protected int speed;
    public void setSpeed(int s){
        speed = s;
        System.out.println("速度を"+speed+"にしました。");
    }
    public void setSpeed(){
        System.out.println("速度はまだありません");
    }
    //抽象メソッド
    public abstract void show();
}