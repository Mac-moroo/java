class Car{
    //フィールド
    private int num;
    private double gas;

    //コンストラクタ 初期化？ 何も引数がなかった時に使われるのか？
    public Car(){
        num = 0;
        gas = 0.0;
        System.out.println("車を作成しました。");
    }
    public Car(int n,double g){
        this.num = n;
        this.gas = g;
        System.out.println("ナンバー"+this.num+"ガソリン量"+this.gas+"の車を作成しました。");
    }

    //車のナンバーとガソリン量の設定メソッド
    public void setCar(int n,double g){
        this.num = n;
        this.gas = g;
        System.out.println("ナンバーを"+this.num+"にガソリン量を"+this.gas+"にしました。");
    }
    //車のナンバーとガソリン量を表示するメソッド
    public void show(){
        System.out.println("車のナンバーは"+num+"です。");
        System.out.println("ガソリン量は"+gas+"です。");
    }
}