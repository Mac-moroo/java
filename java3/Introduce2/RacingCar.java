class RacingCar extends Car{ //extendsはサブクラスの宣言
    //追加のフィールド
    private int course;

    //サブクラスのコンストラクタ
    public RacingCar(){
        course = 0;
        System.out.println("レーシングカーを作成しました。");
    }
    public RacingCar(int n,double g,int c){
        super(n,g);
        this.course = c;
        System.out.println("コース番号"+this.course+"のレーシングカーを作成しました。");
    }

    //追加のメソッド　コースを設定するメソッド
    public void setCourse(int c){
        this.course = c;
        System.out.println("コース番号を"+this.course+"に設定しました。");
    }
}