/**
 * ジェネリクスを使用したクラス
 * @param <T>
 */


class Sample<T>{
    T data1;

    // コンストラクタ
    public Sample(T data){
        this.data1 = data;
    }
    
    //メソッド
    public T getData1(){
        return data1;
    }
}
