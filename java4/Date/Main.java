import java.util.Date;

class Main{
    public static void main(String[] args) {
        Date now = new Date();
        System.out.println(now);
    }



    public Date stringToDate(String string) {
        return new SimpleDateFormat("yyyy/MM/dd HH:mm:ss.SSS").parse(string);
    }
    ​
    public String dateToString(Date date) {
        return new SimpleDateFormat("yyyy/MM/dd HH:mm:ss.SSS").format(date);
    }
}