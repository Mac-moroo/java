import java.util.Calendar;;

public class CurrentDate{
/* 現在の日付を取得 */
    private Calendar currentDate = Calendar.getInstance();
    /** 曜日の値 */
    private int yobi = 0;
    /** 日時の制限の値 */
    final int SEIGEN_NICHIJI = 3;
    boolean donichi;

/**
 * 
 */



/**
 * 今日の日付を出力する。
 */
public void outputToday(){
     System.out.println("今日の日付は"+getToday()+"日です！");
 }
/**
 *今日の日付を取得する。
 */
private int getToday(){
    return currentDate.get(Calendar.DATE);
}

/**
 * 今日から3日後までが、土曜日か日曜日判断し、土日かどうかを返す
 */
private boolean todayKaraSeigenNichijiMadehandan(){

    for(int i = 0; i<SEIGEN_NICHIJI; i++){
        if(getYobi()== Calendar.SATURDAY || getYobi()== Calendar.SUNDAY){
            donichi = true;
            break;
        }
        currentDate.add(Calendar.DAY_OF_MONTH,1);
    }
    System.out.print("向こう"+SEIGEN_NICHIJI+"日以内に土曜日、日曜日は");
    return donichi;
}
 /**
 * 曜日を取得する。
 * @return yobi 曜日
 */
 private int getYobi(){
    yobi =(currentDate.get(Calendar.DAY_OF_WEEK));
    return yobi;
 }

/**
 * 土日か判断して、文言を出力する。
 */
public void donichiHantei(){
    // 判断を行い結果を得る
    boolean donichi = this.todayKaraSeigenNichijiMadehandan();
    if(donichi){
        System.out.println("含まれています！");
    }else{
        System.out.println("含まれていません...");
    }
}

}