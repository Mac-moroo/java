import java.util.Date;
import java.util.Calendar;

public class Today{
    public static void main(String[] args) {

        /* 現在の日付を取得 */
        Calendar today = Calendar.getInstance();
        Calendar tomorrow = Calendar.getInstance();
        Calendar dayAfterTomorrow = Calendar.getInstance();
        Calendar twoDaysAfterTomorrow = Calendar.getInstance();


        /** 今日の日時を取得 */
        System.out.println("今日の日付は"+today.get(Calendar.DATE)+"日です！");

        
        /** 明日の日時を取得 */
        tomorrow.get(Calendar.DATE);
        tomorrow.add(Calendar.DAY_OF_MONTH, 1);
        /** 明日の曜日を取得 */
        int tommorrowYobi = (tomorrow.get(Calendar.DAY_OF_WEEK));


        /** 明後日の日時を取得 */
        dayAfterTomorrow.get(Calendar.DATE);
        dayAfterTomorrow.add(Calendar.DAY_OF_MONTH, 2);
        /** 明後日の曜日を取得 */
        int dayAfterTomorrowYobi = (dayAfterTomorrow.get(Calendar.DAY_OF_WEEK));


         /** 明々後日の日時を取得 */
        twoDaysAfterTomorrow.get(Calendar.DATE);
        twoDaysAfterTomorrow.add(Calendar.DAY_OF_MONTH, 3);
        /** 明々後日の曜日を取得 */
        int twoDaysAfterTomorrowYobi = (twoDaysAfterTomorrow.get(Calendar.DAY_OF_WEEK));


        /** 今日から向こう3日以内の場合 */
        if(today.before(twoDaysAfterTomorrow)){
            /** 今日、明日、明後日、明々後日の曜日が、土曜日または日曜日か判断する */
            System.out.print("今日から向こう3日以内に土曜日、日曜日は");
            if(tommorrowYobi == Calendar.SATURDAY 
             || tommorrowYobi == Calendar.SUNDAY 
             || dayAfterTomorrowYobi == Calendar.SATURDAY
             || dayAfterTomorrowYobi == Calendar.SUNDAY
             || twoDaysAfterTomorrowYobi == Calendar.SATURDAY
             || twoDaysAfterTomorrowYobi == Calendar.SUNDAY){
                System.out.println("含まれます！");
            }else{
                System.out.println("含まれません...");
            }
        }
    }
}