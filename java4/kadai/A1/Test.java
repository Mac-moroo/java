import java.util.Calendar;

public class Test{

    /* 現在の日付を取得 */
    static Calendar today = Calendar.getInstance();
    static Calendar afterDay = Calendar.getInstance();

    /** 曜日の値 */
    static int yobi = 0;
    /** 日時の制限の値 */
    static int SEIGEN_NICHIJI = 3;


    /**
     * 今日の日付を出力する。
     * @return 今日の日付
     */
    private static int todayDate(){
        return today.get(Calendar.DATE);
    }

    /**
     * 今日から指定された日付までの曜日に、土日が含まれているか繰り返す。
     * 
     */
     private static void todayKaraAfterDayWokurikaesi(){
        afterDay.get(Calendar.DATE);
        afterDay.add(Calendar.DAY_OF_MONTH, (SEIGEN_NICHIJI)-1);
        System.out.print(todayDate()+"日から向こう"+SEIGEN_NICHIJI+"日以内に土曜日、日曜日は");

        while(today.before(afterDay)){
            today.add(Calendar.DAY_OF_MONTH,1);
            yobi = (today.get(Calendar.DAY_OF_WEEK));
            if(doniti()){
                break;
            }
        }
        yobiHandan();
     }



    /**
     * ‪曜日が土曜、または日曜日か判断する。
     * @return 曜日が土曜、又は日曜日の場合にtrueを返し、それ以外の場合はfalseを返す。
     */
    private static boolean yobiHandan(){
        if(doniti()){
            System.out.println("含まれます！");
            return true;
        }else{
            System.out.println("含まれません...");
            return false;
        }
    }
    /**
     * 曜日が土曜、または日曜日であることを返す。
     * @return yobi 曜日
     */
    private static boolean doniti(){
        return yobi == Calendar.SATURDAY || yobi == Calendar.SUNDAY;
    }



    public static void main(String[] args) {
        System.out.println("今日の日付は"+todayDate()+"日です。");
        todayKaraAfterDayWokurikaesi();

    }

}