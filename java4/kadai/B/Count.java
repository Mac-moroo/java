import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Random;
import java.util.Scanner;

public class Count{

    private static Scanner scanner = new Scanner(System.in);
    /** ランダムなカウント */
    private static int randomCount;
    /** 入力したカウント */
    private static int inputCount;
    /** 開始時間 */
    private static BigDecimal startTime;
    /** 終了時間 */
    private static BigDecimal endTime;
    /** エンター入力 */
    private static String enter;
    /** 秒 */
    private static double secondsCount;

    /**
     * 問題文を返す
     * @return 問題文
     */
    public String question(){
        return "早打ちチャレンジ ! are you ready ?";
    }

    /** 
     * ランダムなカウントを返す 
     * @return randomCount ランダムカウント
     */
    public int outputRandomCount(){
        Random random = new Random();
        this.randomCount = random.nextInt(lastCount()+1)+startCount();
        return this.randomCount;
    }
    
    /**
     * 10桁の最初の数値を返す
     * @return　startCount
     */
    private int startCount(){
        BigInteger startBigCount = new BigInteger("1000000000");
        int startCount = startBigCount.intValue();
        return startCount;
    }

    /**
     * 10桁の最後の数値を返す
     * @return　endCount
     */
    private int lastCount(){
        BigInteger endBigCount = new BigInteger("9999999999");
        int endCount = endBigCount.intValue();
        return endCount;
    }

    /**
     * 入力したカウントを返す
     * @return inputCount　入力カウント
     */
    public int inputCount(){
        return this.inputCount = scanner.nextInt();
    }

    /**
     * 開始時間を返す 
     * @return startTime BigDecimal型の開始時間
     */
    public BigDecimal startTime(){
        long milliStartTime = System.currentTimeMillis();
        this.startTime = new BigDecimal(milliStartTime);
        return startTime;
    }

    /**
     * 終了時間を返す
     * @return endTime BigDecimal型の終了時間
     */
    public BigDecimal endTime(){
        // 終了時間をミリ秒で取得
        long milliEndTime = System.currentTimeMillis();
        this.endTime = new BigDecimal(milliEndTime);
        return this.endTime;
    } 

    /** エンターを押す機能を返す 
     * @return enter エンター押下
     */
    public String pushEnter(){
        return this.enter = scanner.nextLine();
    }

    /** 
     * 正解か不正解かの判定を返す
     * @return ランダムなカウントと入力カウントが正しい場合、または正しくない場合、falseを返す。
   　*/
    public boolean answer(){
        if(randomCount == inputCount){
            System.out.print("COLLECT ! "+secondsCount()+"秒かかりました！");  
            pushEnter();
            return false;
        }else{
            System.out.print("MISS ! ");
            pushEnter();
            return false;
        }
   
    }
    /**
     * 終了時間を秒数で表示する
     * @return secondsCount 秒数
     */
    private double secondsCount(){
        // 処理時間 ミリ秒を取得
        BigDecimal tmsec = this.endTime.subtract(this.startTime);
        // 処理時間 秒に変換 (1ミリ秒は1/1000秒のため。)
        return this.secondsCount = tmsec.doubleValue() / 1000.0;
    }

}