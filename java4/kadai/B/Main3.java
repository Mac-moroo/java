import java.math.BigDecimal;
import java.util.Scanner;
public class Main3 {
    public static void main( String[] args ) {

        BigDecimal LAST_COUNT = new BigDecimal(100000000);
        int intLastCount = LAST_COUNT.intValue();
        Scanner sc = new Scanner(System.in);
        
            System.out.println("早打ちチャレンジ ! are you ready ?");
            int randomCount = (int)(Math.random()*intLastCount);
            System.out.println(randomCount);

            // 開始時間をミリ秒で取得
            long timeStart = System.currentTimeMillis();
            // 数値の入力
            int inputCount = sc.nextInt();

            if(randomCount == inputCount){
                // 終了時間をミリ秒で取得
                long timeEnd = System.currentTimeMillis();
                // 処理時間 ミリ秒を取得
                long tmsec = timeEnd - timeStart;
                // 処理時間 秒に変換 (1ミリ秒は1/1000秒のため。)
                double tsec = (double)tmsec / 1000.0;

                System.out.println("COLLECT ! "+tsec+"秒かかりました");            
            }else{
                System.out.println("MISS ! ");
            }
        
    }    
}
/**
 * 参考サイト
 * https://talavax.com/timemeasurement.html
 */