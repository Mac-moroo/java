import org.apache.commons.lang3.StringUtils;

class Main{
    public static void main(String[] args) {
        if(args.length > 0){
            System.out.println(StringUtils.reverse(args[0]));
        }
    }
}