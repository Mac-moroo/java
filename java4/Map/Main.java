import java.util.Map;
import java.util.HashMap;

class Main{
    public static void main(String[] args) {
        Map<String, Employee> map = new HashMap<>();
        map.put("191",new Employee("ジョン",30));
        map.put("245", new Employee("スミス", 28));

        map.get("191");
        System.out.println(map);

        Map<String,String> color = new HashMap<>();
        color.put("a", "赤");
        color.put("b", "青");
        color.put("c", "黄");
        System.out.println(color);
    }
}